package dev.rubio.controller;

import dev.rubio.data.MarketItemData;
import dev.rubio.models.MarketItem;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemController {

    Logger logger = LoggerFactory.getLogger(ItemController.class);
    private MarketItemData data = new MarketItemData();

    // this method handles GET /items
    public void handleGetItemsRequest(Context ctx){
        //ctx.result(data.getAllItems().toString());}
        logger.info("getting all items");
        ctx.json(data.getAllItems()); // json method converts object to JSON
    }

    public void handleGetItemByIdRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            MarketItem item = data.getItemById(idInput);
            if (item == null) {
                //                ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("getting item with id: " + idInput);
                ctx.json(item);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewItem(Context ctx){
        MarketItem item = ctx.bodyAsClass(MarketItem.class);
        logger.info("adding new item: "+item);
        data.addNewItem(item);
        ctx.status(201);
    }

    public void handleDeleteById(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting record with id: "+idInput);
            data.deleteItem(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }

    }

}
